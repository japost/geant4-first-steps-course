Task BX - Electromagnetic shower in PbWO4 crystal
=================================================

# Day 2 Exercises - Home work
-----------------------------
These exercises will be carried out in ExDay2

### Material 
First you can finish exercises 3 and 4 in [Task-B2](./Task-B2.md)

Change the target material to PbWO4 with density  8.280 g/cm3

### Generator 

Please revisit tasks 2 and 3 in [Task-B3](./Task-B3.md)

Change the primary generator class to make the primary particle:
   - gamma
   - energy    10 GeV
   - position  -5 cm   -5 cm  -5 cm
   - direction   1      1      1 

# See the EM shower in a larger volume
Visualize one or a few events.  You should see the electromagnetic shower.

#### Resize the world
Change target and world volume:
- World  size 30 cm x 10 cm x 10 cm 
- Target size 26 cm x  6 cm x  6 cm

- Change the initial position and direction
   - position  -25 cm   -5 cm  -5 cm
   - direction   1      0      0 

Create the shower for 10 or 100 gammas (events)

Compare with the case of primary muon
 - either in Generator Action
 - or interactively in using /gun/particle mu-
  
Increase muon energy to 100 GeV

## More on materials in basic example B1 - optional

From now on, we will use the application in batch mode. 

Exercise M5
-----------
This uses the Geant4 basic example B1 application.
You can create a copy as follows (or find the one you created before the course or in day1 instead.)

```shell
cd ~/g4
mkdir -p examples/basic
cd examples/basic

cp -r $G4EXAMPLES/basic/B1 .
cd B1

mkdir build
cd build
cmake ..
make -j 3
```

- a. Identify and investigate the material definitions in the B1 application B1DetectorConstruction::Construct() method!
- b. Print the Envelope material with all its properties (use G4cout)!
- c. Change the Envelope material from Water to liquid Argon using the NIST material data base.
- d. Define your own Water material instead of the predefined NIST one (see   slide #9) and use it as the Envelop material.


Note: this example can also be run in batch mode - if you call it with a macro file:
```shell
./yourMainApplication run1.mac
```
with run1.mac containing:
```
/tracking/verbose 1
/run/initialize
/gun/particle e-
/gun/energy   3 GeV
/run/beamOn  3
```

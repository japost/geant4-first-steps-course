Day 2 topics
============

Initial items
-------------
- Review homework & answer questions
- Follow-up concepts from homework - extended concepts
- Reset set-up of hands-on

Starting point
--------------
We will start the exercises in day2/ExDay2

Please download the tarball at [Day2-v2.tar](https://indico.cern.ch/event/1014059/contributions/4307648/)
```
cd ~/g4
tar xvf ~/Downloads/Day2-v2.tar
```

New topics
----------
- [Task B1](Task-B1.md) Visualisation
- [Task B2](Task-B2.md) Materials
- [Task B3](Task-B3.md) Generate primaries

Hands-on topics: same 3
---------------

Homework
--------
- [Homework task](./Task-BX.md) More exercises

Developers (proposed):
---------------------
- Visualisation: reuse Mihaly's 2020 material - John
- Materials: Vladimir 
- Primaries: Vladimir + John

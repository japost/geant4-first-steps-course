Primary generation: Hands-on
============================

We will keep using the source code in exDay2/ in these exercises.
A class to generate the primary particles of an event is mandatory.
The goal is to exercise the different functionality for the generator class.

Exercise 1.
-----------
Run the application in interactive mode.
- a. using built-in /gun/ UI commands change primary particle type
     from e- to e+, proton, mu-, gamma 
- b. change energy of the primary
- c. run in verbose mode

Exercise 2.
-----------
Change primary particle parameters in YourPrimaryGenerator class' SetDefaultKinematic() method:
- a. define primary e+
- b. define energy 10 MeV
- c. define position at one of the borders of the world volume, say (-5.0, 0, 0).  Don't forget the units.
- d. define direction towards the middle of the target.

Run a few events, including with 
```
/tracking/verbose 1
```
and also confirm the effect of this method on the results of
```
cd /gun
?particle
?energy
?position
?direction
```
Change one or more of these values
```
/gun/particle mu+
/gun/energy  10 MeV
```
and see whether the particles which are generated and tracked change when you run more events:
```
/tracking/verbose 1
/run/verbose 1
/event/verbose 1
# Make the event verbose, so that you can see the start of each event easily
/run/beamOn 5
```
Don't type the the line starting with '#'.  It is just to make the commands more readable.  

( Note: A line starting with '#' can be used as a comment when running in 'batch' mode. When commands are read from that file, lines starting with # will be ignored, and the content of lines beyond a '#' will be ignored. )

Exercise 3
----------
Change primary particle parameters in YourPrimaryGenerator class's GeneratePrimaries() method:
- a. define primary e+
- b. define position on the surface of the target
- c. define random position on the surface for each new primary

Repeat the same process as in Exercise 2 to check whether changing the GeneratePrimaries method has a different effect.

Can you guess whether the effect will be the same or different and why ?
Which method is called at the start of each event, in order to generate primaries ? 

Where is the SetDefaultKinematic() method called ?  ( Note that this is a method crafted for this application.  It is not part of the interface of G4VUserPrimaryGenerator interface. )



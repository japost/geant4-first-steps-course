Day 4 topics
============

Initial items
-------------
- Review homework & answer questions
- Follow-up concepts from homework - extended concepts
- Reset set-up of hands-on

New topics
----------
- Messengers
- EM Physics
- Hadronic Physics

Hands-on
--------
Please download the tarball [Day4-v2.tar](https://indico.cern.ch/event/1014059/contributions/4307777/attachments/2251157/3823718/Day4-v2.tar) and extract it:
```shell
cd ~/g4
tar xf ~/Downloads/Day4-v2.tar
ls -l day4
cd day4
```

- [Task D1](./Task-D1.md) Messenger - how to create one 
   - Detailed instructions are included with the [solution](./Task-D1-Solution.md))
- [Task D2](./Task-D2.md) EM physics exercise
- [Task D3](./Task-D3.md) Hadronic physics exercise

Homework
--------
Task [DH](Task-DH.md)
- Continue one of the aspects
- Study an extended example

Developers:
----------
Messengers: Mihaly
EM Physics: Mihaly/Vladimir
Hadronic Physics: Alberto

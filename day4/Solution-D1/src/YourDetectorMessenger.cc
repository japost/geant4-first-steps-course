
#include "YourDetectorMessenger.hh"

#include "YourDetectorConstruction.hh"

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4String.hh"


YourDetectorMessenger::YourDetectorMessenger(YourDetectorConstruction* det)
:   G4UImessenger(),
    fYourDetector(det),
    fDirCMD(nullptr),
    fTargetMaterialCMD(nullptr)
{
    // create the "det" command directory first then add commands
    fDirCMD = new G4UIdirectory("/yourApp/det/");
    fDirCMD->SetGuidance("UI commands specific to the detector construction of this application");
    //
    // UI command to set the target material (NIST material i.e. with G4_ prefix)
    fTargetMaterialCMD  = new G4UIcmdWithAString("/yourApp/det/setTargetMaterial",this);
    fTargetMaterialCMD->SetGuidance("Sets the Material of the Target.");
    fTargetMaterialCMD->SetParameterName("TagetMaterial",false);
    fTargetMaterialCMD->AvailableForStates(G4State_PreInit, G4State_Idle);
//    fTargetMaterialCMD->SetToBeBroadcasted(false);
}


YourDetectorMessenger::~YourDetectorMessenger()
{
  delete fTargetMaterialCMD;
  delete fDirCMD;
}


void YourDetectorMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
    // set tartget material name
    if (command == fTargetMaterialCMD)
    {
        fYourDetector->SetTargetMaterial(newValue);
   }
}

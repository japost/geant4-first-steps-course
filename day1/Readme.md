Lecture topics
==============
- What is Geant4 ?  
  - What is the difference between a tool and a toolkit / library?
- First steps: describing your detector geometry
  - Key geometry concepts

Note: We assume familiary with basics of C++ , but try to point out some aspects as we use them.   We target this intermediate level of expected C++ knowledge, expecting it will be neither too low nor too high for participants.

Getting the exercises - day 1
=============================
- Here is how to files for the [tasks and exercise](./How-to-get-day1.md)
- You need the task files 'Task-A0.md' to 'Task-A3.md' plus the directory 'ExDay1' to start

Hands-on
========
- [Task-A0](./Task-A0.md) Explore the Geant4 Virtual Machine - and get these exercises
- [Task-A1](./Task-A1.md) What is a Geant4 application and how to use it ?
    ** Goals: familiarise with program main, contents of directories,
       compiling and running an executable
- [Task-A2](./Task-A2.md) Writing our own application.
    ** Main program + Adding a volume to the world
- [Task-A3](./Task-A3.md) Quick tour of key resources for Geant4 -
       documentation, doxygen, examples

Homework 
========
- [Task-AX](./Task-AX.md) Add a tube to the setup from [A2](./Task-A2.md)

Solutions
=========
- There are solutions for each task in source directories. You can compare your code with it, once you have finished.

Developers: John Apostolakis + Gabriele Cosmo

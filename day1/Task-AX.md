Task AX - Adding a "disc" volume
================================

Exercise "ExDay1" - Home work
----------------
- Starting from the code in "ExDay1" directory, including the solution for
  Task A2, edit the YourDetectorConstruction.cc file:
  - Add inclusion of header "G4Tubs.hh"
- Just before section III, at the end, define a disc (full unsegmented tube)
  of dimensions:
  - Radius: 5.5 mm
  - Z: 0.5 mm
- Define its logical volume:
  - Define a pointer 'discLogical' to G4LogicalVolume and give
    "logic-Disc" as its name
  - Associate to it the same target material already defined above
- Place the tube in the world volume:
  - Centered at: X= 0, Y= 0, Z= 0.65 cm
  - With no rotation
- Compile and execute
  - Visualise your detector

Exploring more user commands
----------------------------
- Explore other 'tracking' verbosity levels
```
/gun/energy 5 MeV
/tracking/verbose 2
/run/beamOn 1
```
You can look at other values (up to 6):
```
/tracking/verbose 6
/run/beamOn 1 
```
and reset to the 'basic' verbosity or none.
```
/tracking/verbose 1
/tracking/verbose 0
```

Optional
========

Check more Geant4 environment variable
--------------------------------------
```shell
env | grep G4LEDATA

env | grep G4 | grep -v DATA
```

How did we get the Geant4 environment?
--------------------------------------
Investigate how the Geant4 configuration was introduced into the VM.

Hint: look at the file .cshrc in your home directory

